#+TITLE: FreeCAD Electrical Workbench

This workbench is designed as an extension of Arch workbench to cover
electrical wiring in buildings.

* License

LGPL2+

* Documentation

  + [[https://wiki.freecadweb.org/Workbench_creation][Workbench creation]] at FreeCAD wiki
